Not used. can clone this repo on uss - f/w issue? see ghe 

# MortgageApplication
This version of the MortgageApplication sample is designed to be built by zAppBuild. 

**Example showing how to build all programs in MortgageApplication**
```
$DBB_HOME/bin/groovyz build.groovy --workspace /u/build/repos/dbb-zappbuild/samples --application MortgageApplication --outDir /u/build/out --hlq BUILD.MORTAPP --fullBuild
```
See [BUILD.md](../../../BUILD.md) for additional information about building applications using zAppBuild.
